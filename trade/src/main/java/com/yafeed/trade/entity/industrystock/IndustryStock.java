package com.yafeed.trade.entity.industrystock;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

@Table(name = "industry_stock")
public class IndustryStock implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "industry_stock_id")
    private Long industryStockId;

    /**
     * 股票代码
     */
    @Column(name = "stock_code")
    private String stockCode;

    /**
     * 股票名称
     */
    @Column(name = "stock_name")
    private String stockName;

    /**
     * 当前价
     */
    @Column(name = "current_price")
    private BigDecimal currentPrice;

    /**
     * 昨收
     */
    @Column(name = "pre_price")
    private BigDecimal prePrice;

    /**
     * 涨跌额
     */
    @Column(name = "change_amount")
    private BigDecimal changeAmount;

    /**
     * 涨跌幅
     */
    @Column(name = "change_range")
    private BigDecimal changeRange;

    /**
     * 最高价
     */
    @Column(name = "high_price")
    private BigDecimal highPrice;

    /**
     * 最低价
     */
    @Column(name = "low_price")
    private BigDecimal lowPrice;

    /**
     * 交易量
     */
    @Column(name = "trade_volume")
    private Long tradeVolume;

    /**
     * 交易额
     */
    @Column(name = "trade_price")
    private Long tradePrice;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return industry_stock_id - 主键
     */
    public Long getIndustryStockId() {
        return industryStockId;
    }

    /**
     * 设置主键
     *
     * @param industryStockId 主键
     */
    public void setIndustryStockId(Long industryStockId) {
        this.industryStockId = industryStockId;
    }

    /**
     * 获取股票代码
     *
     * @return stock_code - 股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置股票代码
     *
     * @param stockCode 股票代码
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode == null ? null : stockCode.trim();
    }

    /**
     * 获取股票名称
     *
     * @return stock_name - 股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     *
     * @param stockName 股票名称
     */
    public void setStockName(String stockName) {
        this.stockName = stockName == null ? null : stockName.trim();
    }

    /**
     * 获取当前价
     *
     * @return current_price - 当前价
     */
    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    /**
     * 设置当前价
     *
     * @param currentPrice 当前价
     */
    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    /**
     * 获取昨收
     *
     * @return pre_price - 昨收
     */
    public BigDecimal getPrePrice() {
        return prePrice;
    }

    /**
     * 设置昨收
     *
     * @param prePrice 昨收
     */
    public void setPrePrice(BigDecimal prePrice) {
        this.prePrice = prePrice;
    }

    /**
     * 获取涨跌额
     *
     * @return change_amount - 涨跌额
     */
    public BigDecimal getChangeAmount() {
        return changeAmount;
    }

    /**
     * 设置涨跌额
     *
     * @param changeAmount 涨跌额
     */
    public void setChangeAmount(BigDecimal changeAmount) {
        this.changeAmount = changeAmount;
    }

    /**
     * 获取涨跌幅
     *
     * @return change_range - 涨跌幅
     */
    public BigDecimal getChangeRange() {
        return changeRange;
    }

    /**
     * 设置涨跌幅
     *
     * @param changeRange 涨跌幅
     */
    public void setChangeRange(BigDecimal changeRange) {
        this.changeRange = changeRange;
    }

    /**
     * 获取最高价
     *
     * @return high_price - 最高价
     */
    public BigDecimal getHighPrice() {
        return highPrice;
    }

    /**
     * 设置最高价
     *
     * @param highPrice 最高价
     */
    public void setHighPrice(BigDecimal highPrice) {
        this.highPrice = highPrice;
    }

    /**
     * 获取最低价
     *
     * @return low_price - 最低价
     */
    public BigDecimal getLowPrice() {
        return lowPrice;
    }

    /**
     * 设置最低价
     *
     * @param lowPrice 最低价
     */
    public void setLowPrice(BigDecimal lowPrice) {
        this.lowPrice = lowPrice;
    }

    /**
     * 获取交易量
     *
     * @return trade_volume - 交易量
     */
    public Long getTradeVolume() {
        return tradeVolume;
    }

    /**
     * 设置交易量
     *
     * @param tradeVolume 交易量
     */
    public void setTradeVolume(Long tradeVolume) {
        this.tradeVolume = tradeVolume;
    }

    /**
     * 获取交易额
     *
     * @return trade_price - 交易额
     */
    public Long getTradePrice() {
        return tradePrice;
    }

    /**
     * 设置交易额
     *
     * @param tradePrice 交易额
     */
    public void setTradePrice(Long tradePrice) {
        this.tradePrice = tradePrice;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}