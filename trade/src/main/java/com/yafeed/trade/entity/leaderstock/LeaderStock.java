package com.yafeed.trade.entity.leaderstock;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

public class LeaderStock implements Serializable {
    /**
     * 主键
     */
    @Id
    @Column(name = "leader_stock_id")
    private Long leaderStockId;

    /**
     * 行业表主键
     */
    @Column(name = "industry_id")
    private Long industryId;

    /**
     * 行业编码
     */
    @Column(name = "industry_code")
    private String industryCode;

    /**
     * 股票代码
     */
    @Column(name = "stock_code")
    private String stockCode;

    /**
     * 股票名称
     */
    @Column(name = "stock_name")
    private String stockName;

    /**
     * 当前价
     */
    @Column(name = "current_price")
    private BigDecimal currentPrice;

    /**
     * 涨跌幅
     */
    @Column(name = "change_range")
    private BigDecimal changeRange;

    /**
     * 涨跌额
     */
    @Column(name = "change_amount")
    private BigDecimal changeAmount;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取主键
     *
     * @return leader_stock_id - 主键
     */
    public Long getLeaderStockId() {
        return leaderStockId;
    }

    /**
     * 设置主键
     *
     * @param leaderStockId 主键
     */
    public void setLeaderStockId(Long leaderStockId) {
        this.leaderStockId = leaderStockId;
    }

    /**
     * 获取行业表主键
     *
     * @return industry_id - 行业表主键
     */
    public Long getIndustryId() {
        return industryId;
    }

    /**
     * 设置行业表主键
     *
     * @param industryId 行业表主键
     */
    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    /**
     * 获取行业编码
     *
     * @return industry_code - 行业编码
     */
    public String getIndustryCode() {
        return industryCode;
    }

    /**
     * 设置行业编码
     *
     * @param industryCode 行业编码
     */
    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode == null ? null : industryCode.trim();
    }

    /**
     * 获取股票代码
     *
     * @return stock_code - 股票代码
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * 设置股票代码
     *
     * @param stockCode 股票代码
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode == null ? null : stockCode.trim();
    }

    /**
     * 获取股票名称
     *
     * @return stock_name - 股票名称
     */
    public String getStockName() {
        return stockName;
    }

    /**
     * 设置股票名称
     *
     * @param stockName 股票名称
     */
    public void setStockName(String stockName) {
        this.stockName = stockName == null ? null : stockName.trim();
    }

    /**
     * 获取当前价
     *
     * @return current_price - 当前价
     */
    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    /**
     * 设置当前价
     *
     * @param currentPrice 当前价
     */
    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }

    /**
     * 获取涨跌幅
     *
     * @return change_range - 涨跌幅
     */
    public BigDecimal getChangeRange() {
        return changeRange;
    }

    /**
     * 设置涨跌幅
     *
     * @param changeRange 涨跌幅
     */
    public void setChangeRange(BigDecimal changeRange) {
        this.changeRange = changeRange;
    }

    /**
     * 获取涨跌额
     *
     * @return change_amount - 涨跌额
     */
    public BigDecimal getChangeAmount() {
        return changeAmount;
    }

    /**
     * 设置涨跌额
     *
     * @param changeAmount 涨跌额
     */
    public void setChangeAmount(BigDecimal changeAmount) {
        this.changeAmount = changeAmount;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}