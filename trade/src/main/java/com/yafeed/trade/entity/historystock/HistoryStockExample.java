package com.yafeed.trade.entity.historystock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HistoryStockExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public HistoryStockExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andHistoryStockIdIsNull() {
            addCriterion("history_stock_id is null");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdIsNotNull() {
            addCriterion("history_stock_id is not null");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdEqualTo(Long value) {
            addCriterion("history_stock_id =", value, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdNotEqualTo(Long value) {
            addCriterion("history_stock_id <>", value, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdGreaterThan(Long value) {
            addCriterion("history_stock_id >", value, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdGreaterThanOrEqualTo(Long value) {
            addCriterion("history_stock_id >=", value, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdLessThan(Long value) {
            addCriterion("history_stock_id <", value, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdLessThanOrEqualTo(Long value) {
            addCriterion("history_stock_id <=", value, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdIn(List<Long> values) {
            addCriterion("history_stock_id in", values, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdNotIn(List<Long> values) {
            addCriterion("history_stock_id not in", values, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdBetween(Long value1, Long value2) {
            addCriterion("history_stock_id between", value1, value2, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andHistoryStockIdNotBetween(Long value1, Long value2) {
            addCriterion("history_stock_id not between", value1, value2, "historyStockId");
            return (Criteria) this;
        }

        public Criteria andTradeDayIsNull() {
            addCriterion("trade_day is null");
            return (Criteria) this;
        }

        public Criteria andTradeDayIsNotNull() {
            addCriterion("trade_day is not null");
            return (Criteria) this;
        }

        public Criteria andTradeDayEqualTo(Date value) {
            addCriterion("trade_day =", value, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayNotEqualTo(Date value) {
            addCriterion("trade_day <>", value, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayGreaterThan(Date value) {
            addCriterion("trade_day >", value, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayGreaterThanOrEqualTo(Date value) {
            addCriterion("trade_day >=", value, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayLessThan(Date value) {
            addCriterion("trade_day <", value, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayLessThanOrEqualTo(Date value) {
            addCriterion("trade_day <=", value, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayIn(List<Date> values) {
            addCriterion("trade_day in", values, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayNotIn(List<Date> values) {
            addCriterion("trade_day not in", values, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayBetween(Date value1, Date value2) {
            addCriterion("trade_day between", value1, value2, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andTradeDayNotBetween(Date value1, Date value2) {
            addCriterion("trade_day not between", value1, value2, "tradeDay");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNull() {
            addCriterion("stock_code is null");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNotNull() {
            addCriterion("stock_code is not null");
            return (Criteria) this;
        }

        public Criteria andStockCodeEqualTo(String value) {
            addCriterion("stock_code =", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotEqualTo(String value) {
            addCriterion("stock_code <>", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThan(String value) {
            addCriterion("stock_code >", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThanOrEqualTo(String value) {
            addCriterion("stock_code >=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThan(String value) {
            addCriterion("stock_code <", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThanOrEqualTo(String value) {
            addCriterion("stock_code <=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLike(String value) {
            addCriterion("stock_code like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotLike(String value) {
            addCriterion("stock_code not like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeIn(List<String> values) {
            addCriterion("stock_code in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotIn(List<String> values) {
            addCriterion("stock_code not in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeBetween(String value1, String value2) {
            addCriterion("stock_code between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotBetween(String value1, String value2) {
            addCriterion("stock_code not between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockNameIsNull() {
            addCriterion("stock_name is null");
            return (Criteria) this;
        }

        public Criteria andStockNameIsNotNull() {
            addCriterion("stock_name is not null");
            return (Criteria) this;
        }

        public Criteria andStockNameEqualTo(String value) {
            addCriterion("stock_name =", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotEqualTo(String value) {
            addCriterion("stock_name <>", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameGreaterThan(String value) {
            addCriterion("stock_name >", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameGreaterThanOrEqualTo(String value) {
            addCriterion("stock_name >=", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameLessThan(String value) {
            addCriterion("stock_name <", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameLessThanOrEqualTo(String value) {
            addCriterion("stock_name <=", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameLike(String value) {
            addCriterion("stock_name like", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotLike(String value) {
            addCriterion("stock_name not like", value, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameIn(List<String> values) {
            addCriterion("stock_name in", values, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotIn(List<String> values) {
            addCriterion("stock_name not in", values, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameBetween(String value1, String value2) {
            addCriterion("stock_name between", value1, value2, "stockName");
            return (Criteria) this;
        }

        public Criteria andStockNameNotBetween(String value1, String value2) {
            addCriterion("stock_name not between", value1, value2, "stockName");
            return (Criteria) this;
        }

        public Criteria andOpenPriceIsNull() {
            addCriterion("open_price is null");
            return (Criteria) this;
        }

        public Criteria andOpenPriceIsNotNull() {
            addCriterion("open_price is not null");
            return (Criteria) this;
        }

        public Criteria andOpenPriceEqualTo(BigDecimal value) {
            addCriterion("open_price =", value, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceNotEqualTo(BigDecimal value) {
            addCriterion("open_price <>", value, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceGreaterThan(BigDecimal value) {
            addCriterion("open_price >", value, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("open_price >=", value, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceLessThan(BigDecimal value) {
            addCriterion("open_price <", value, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("open_price <=", value, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceIn(List<BigDecimal> values) {
            addCriterion("open_price in", values, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceNotIn(List<BigDecimal> values) {
            addCriterion("open_price not in", values, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("open_price between", value1, value2, "openPrice");
            return (Criteria) this;
        }

        public Criteria andOpenPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("open_price not between", value1, value2, "openPrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceIsNull() {
            addCriterion("close_price is null");
            return (Criteria) this;
        }

        public Criteria andClosePriceIsNotNull() {
            addCriterion("close_price is not null");
            return (Criteria) this;
        }

        public Criteria andClosePriceEqualTo(BigDecimal value) {
            addCriterion("close_price =", value, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceNotEqualTo(BigDecimal value) {
            addCriterion("close_price <>", value, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceGreaterThan(BigDecimal value) {
            addCriterion("close_price >", value, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("close_price >=", value, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceLessThan(BigDecimal value) {
            addCriterion("close_price <", value, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("close_price <=", value, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceIn(List<BigDecimal> values) {
            addCriterion("close_price in", values, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceNotIn(List<BigDecimal> values) {
            addCriterion("close_price not in", values, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("close_price between", value1, value2, "closePrice");
            return (Criteria) this;
        }

        public Criteria andClosePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("close_price not between", value1, value2, "closePrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceIsNull() {
            addCriterion("high_price is null");
            return (Criteria) this;
        }

        public Criteria andHighPriceIsNotNull() {
            addCriterion("high_price is not null");
            return (Criteria) this;
        }

        public Criteria andHighPriceEqualTo(BigDecimal value) {
            addCriterion("high_price =", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceNotEqualTo(BigDecimal value) {
            addCriterion("high_price <>", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceGreaterThan(BigDecimal value) {
            addCriterion("high_price >", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("high_price >=", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceLessThan(BigDecimal value) {
            addCriterion("high_price <", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("high_price <=", value, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceIn(List<BigDecimal> values) {
            addCriterion("high_price in", values, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceNotIn(List<BigDecimal> values) {
            addCriterion("high_price not in", values, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("high_price between", value1, value2, "highPrice");
            return (Criteria) this;
        }

        public Criteria andHighPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("high_price not between", value1, value2, "highPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceIsNull() {
            addCriterion("low_price is null");
            return (Criteria) this;
        }

        public Criteria andLowPriceIsNotNull() {
            addCriterion("low_price is not null");
            return (Criteria) this;
        }

        public Criteria andLowPriceEqualTo(BigDecimal value) {
            addCriterion("low_price =", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotEqualTo(BigDecimal value) {
            addCriterion("low_price <>", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceGreaterThan(BigDecimal value) {
            addCriterion("low_price >", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("low_price >=", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceLessThan(BigDecimal value) {
            addCriterion("low_price <", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("low_price <=", value, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceIn(List<BigDecimal> values) {
            addCriterion("low_price in", values, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotIn(List<BigDecimal> values) {
            addCriterion("low_price not in", values, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_price between", value1, value2, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andLowPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_price not between", value1, value2, "lowPrice");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeIsNull() {
            addCriterion("trade_volume is null");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeIsNotNull() {
            addCriterion("trade_volume is not null");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeEqualTo(Long value) {
            addCriterion("trade_volume =", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeNotEqualTo(Long value) {
            addCriterion("trade_volume <>", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeGreaterThan(Long value) {
            addCriterion("trade_volume >", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_volume >=", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeLessThan(Long value) {
            addCriterion("trade_volume <", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeLessThanOrEqualTo(Long value) {
            addCriterion("trade_volume <=", value, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeIn(List<Long> values) {
            addCriterion("trade_volume in", values, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeNotIn(List<Long> values) {
            addCriterion("trade_volume not in", values, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeBetween(Long value1, Long value2) {
            addCriterion("trade_volume between", value1, value2, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andTradeVolumeNotBetween(Long value1, Long value2) {
            addCriterion("trade_volume not between", value1, value2, "tradeVolume");
            return (Criteria) this;
        }

        public Criteria andMa20PriceIsNull() {
            addCriterion("ma20_price is null");
            return (Criteria) this;
        }

        public Criteria andMa20PriceIsNotNull() {
            addCriterion("ma20_price is not null");
            return (Criteria) this;
        }

        public Criteria andMa20PriceEqualTo(BigDecimal value) {
            addCriterion("ma20_price =", value, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceNotEqualTo(BigDecimal value) {
            addCriterion("ma20_price <>", value, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceGreaterThan(BigDecimal value) {
            addCriterion("ma20_price >", value, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ma20_price >=", value, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceLessThan(BigDecimal value) {
            addCriterion("ma20_price <", value, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ma20_price <=", value, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceIn(List<BigDecimal> values) {
            addCriterion("ma20_price in", values, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceNotIn(List<BigDecimal> values) {
            addCriterion("ma20_price not in", values, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ma20_price between", value1, value2, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20PriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ma20_price not between", value1, value2, "ma20Price");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeIsNull() {
            addCriterion("ma20_volume is null");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeIsNotNull() {
            addCriterion("ma20_volume is not null");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeEqualTo(Long value) {
            addCriterion("ma20_volume =", value, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeNotEqualTo(Long value) {
            addCriterion("ma20_volume <>", value, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeGreaterThan(Long value) {
            addCriterion("ma20_volume >", value, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeGreaterThanOrEqualTo(Long value) {
            addCriterion("ma20_volume >=", value, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeLessThan(Long value) {
            addCriterion("ma20_volume <", value, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeLessThanOrEqualTo(Long value) {
            addCriterion("ma20_volume <=", value, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeIn(List<Long> values) {
            addCriterion("ma20_volume in", values, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeNotIn(List<Long> values) {
            addCriterion("ma20_volume not in", values, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeBetween(Long value1, Long value2) {
            addCriterion("ma20_volume between", value1, value2, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andMa20VolumeNotBetween(Long value1, Long value2) {
            addCriterion("ma20_volume not between", value1, value2, "ma20Volume");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}