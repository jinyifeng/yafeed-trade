package com.yafeed.trade.config;

import com.yafeed.trade.common.http.SSLFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @program:
 * @description: 配置信息
 * @author: yanghj
 * @time: 2020-12-13 01:37
 */


@Configuration
public class YafeedConfig {

    @Bean
    public RestTemplate restTemplate() {
        SSLFactory factory = new SSLFactory();
        factory.setReadTimeout(1500);
        //单位为ms
        factory.setConnectTimeout(3000);
        return new RestTemplate(factory);
    }
}
