package com.yafeed.trade.common.utils;

import com.yafeed.trade.common.enums.DateTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @description: 速度限制
 * @author: yanghj
 * @time: 2020-12-21 10:03
 **/
@Setter
@Getter
public class RateUtils {

    /**
     * 令牌数量
     */
    private int counts = 20;
    /**
     * 周期
     */
    private int period = 1;
    /**
     * 桶刷新时间
     */
    private Date refreshTime = DateTypeEnum.YYYY_MM_DD_HH_MM_SS.date();

    /**
     * 桶容量
     */
    private int bucketCap;

    private static RateUtils of() {
        return new RateUtils();
    }


    public static RateUtils of(int counts, int seconds) {
        if (counts <= 0) {
            counts = 20;
        }

        if (seconds <= 0) {
            seconds = 5;
        }
        RateUtils utils = of();
        utils.setCounts(counts);
        utils.setPeriod(seconds);
        utils.setRefreshTime(new Date());
        utils.setBucketCap(counts);
        return utils;
    }


    /**
     * <pre>
     *     是否有令牌
     * </pre>
     *
     * @return
     */
    public boolean hasToken() {
        synchronized (this) {
            Date now = new Date();
            // 计算时间差
            int seconds = subtractSeconds(now, getRefreshTime());
            // 在时间周期内
            if (seconds <= getPeriod()) {
                // 如果有令牌
                if (getBucketCap() <= getCounts() && getBucketCap() > 0) {
                    bucketCap--;
                    return true;
                }
                return false;
            }
            if (getBucketCap() <= 0) {
                resetBucketCap();
            }
            // 重置时间
            resetRefreshTime(now);
            return false;
        }
    }


    /**
     * <pre>
     *     求两个时间差
     * </pre>
     *
     * @param date    减数
     * @param seconds 被减数
     * @return 时间差 (秒)
     */
    private int subtractSeconds(Date date, Date seconds) {
        if (seconds.compareTo(date) > 0) {
            return 0;
        }
        long diff = date.getTime() - seconds.getTime();
        return (int) (diff / 1000);
    }

    private void resetBucketCap() {
        setBucketCap(getCounts());
    }

    private void resetRefreshTime(Date resetTime) {
        setRefreshTime(resetTime);
    }
}
