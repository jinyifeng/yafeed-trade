package com.yafeed.trade.common.pojo.bo;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

/**
 * @description: 分页
 * @author: yanghj
 * @time: 2020-12-22 15:36
 **/
@Setter
@Getter
public class PageBean<T> {

    private int pageNo;

    private int pageSize;

    private String order;

    public PageBean(int pageNo, int pageSize, String order) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.order = order;
    }

    public PageInfo<T> getPageInfo() {
        PageHelper.startPage(getPageNo(), getPageSize(), getOrder());
        return new PageInfo<>(doQuery());
    }

    public List<T> doQuery() {
        return Collections.emptyList();
    }


}
