package com.yafeed.trade.common.tuple;

import lombok.Getter;
import lombok.Setter;

/**
 * @description: 两元组
 * @author: yanghj
 * @time: 2020-12-17 10:05
 **/
@Setter
@Getter
public class TwoTuple<A, B> {
    private A first;
    private B second;


    public static <A, B> TwoTuple of(A a, B b) {
        TwoTuple<A, B> twoTuple = new TwoTuple<>();
        twoTuple.setFirst(a);
        twoTuple.setSecond(b);
        return twoTuple;
    }
}
