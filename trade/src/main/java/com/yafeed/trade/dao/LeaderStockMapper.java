package com.yafeed.trade.dao;

import com.yafeed.trade.entity.leaderstock.LeaderStock;
import com.yafeed.trade.entity.leaderstock.LeaderStockExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface LeaderStockMapper extends Mapper<LeaderStock> {
    long countByExample(LeaderStockExample example);

    int deleteByExample(LeaderStockExample example);

    List<LeaderStock> selectByExample(LeaderStockExample example);

    int updateByExampleSelective(@Param("record") LeaderStock record, @Param("example") LeaderStockExample example);

    int updateByExample(@Param("record") LeaderStock record, @Param("example") LeaderStockExample example);
}