package com.yafeed.trade.dao;

import com.yafeed.trade.entity.industrystock.IndustryStock;
import com.yafeed.trade.entity.industrystock.IndustryStockExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface IndustryStockMapper extends Mapper<IndustryStock> {

    /**
     * <pre>
     *     查询行业个股标识
     * </pre>
     *
     * @param pageNo   页码
     * @param pageSize 每页数量
     * @return
     */
    List<IndustryStock> listDistinctIndustryStockSymbol();


    long countByExample(IndustryStockExample example);

    int deleteByExample(IndustryStockExample example);

    List<IndustryStock> selectByExample(IndustryStockExample example);

    int updateByExampleSelective(@Param("record") IndustryStock record, @Param("example") IndustryStockExample example);

    int updateByExample(@Param("record") IndustryStock record, @Param("example") IndustryStockExample example);
}