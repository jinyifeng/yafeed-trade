package com.yafeed.trade.dao;

import com.yafeed.trade.entity.historystock.HistoryStock;
import com.yafeed.trade.entity.historystock.HistoryStockExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface HistoryStockMapper extends Mapper<HistoryStock> {
    long countByExample(HistoryStockExample example);

    int deleteByExample(HistoryStockExample example);

    List<HistoryStock> selectByExample(HistoryStockExample example);

    int updateByExampleSelective(@Param("record") HistoryStock record, @Param("example") HistoryStockExample example);

    int updateByExample(@Param("record") HistoryStock record, @Param("example") HistoryStockExample example);
}