package com.yafeed.trade.pojo.dto.industry;

import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.entity.industry.Industry;
import com.yafeed.trade.entity.leaderstock.LeaderStock;
import com.yafeed.trade.pojo.bo.stock.LeaderStockBo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description: 新浪行业版块信息
 * @author: yanghj
 * @time: 2020-12-13 12:07
 */

@Setter
@Getter
@ToString
public class IndustryDto implements Serializable {

    private static final long serialVersionUID = 6140232953951213322L;

    /**
     * 主键
     */
    private Long industryId;

    /**
     * 行业名称
     */
    private String industryName;

    /**
     * 行业代码
     */
    private String industryCode;

    /**
     * 公司数量
     */
    private Integer companies;

    /**
     * 平均价格
     */
    private BigDecimal averagePrice;

    /**
     * 涨跌额
     */
    private BigDecimal changeAmount;

    /**
     * 涨跌幅（百分比）
     */
    private BigDecimal changeRange;

    /**
     * 成交量
     */
    private Long tradeVolume;

    /**
     * 成交额
     */

    private Long tradePrice;


    /**
     * 领涨股
     */
    private LeaderStock leaderStock;


    private static IndustryDto of() {
        return new IndustryDto();
    }


    public static IndustryDto of(String industryStr, IdService idService) {
        IndustryDto industryDto = of();
        industryDto.setIndustryId(idService.genId());

        String[] serNo = industryStr.split(",");

        industryDto.setIndustryCode(serNo[0]);
        industryDto.setIndustryName(serNo[1]);
        industryDto.setCompanies(Integer.parseInt(serNo[2]));
        industryDto.setAveragePrice(new BigDecimal(serNo[3]));
        industryDto.setChangeAmount(new BigDecimal(serNo[4]));
        industryDto.setChangeRange(new BigDecimal(serNo[5]));
        industryDto.setTradeVolume(Long.parseLong(serNo[6]));
        industryDto.setTradePrice(Long.parseLong(serNo[7]));

        LeaderStock stock = LeaderStockBo.of().or();
        stock.setLeaderStockId(idService.genId());
        stock.setIndustryId(industryDto.getIndustryId());
        stock.setIndustryCode(industryDto.getIndustryCode());
        stock.setStockCode(serNo[8]);
        stock.setChangeRange(new BigDecimal(serNo[9]));
        stock.setCurrentPrice(new BigDecimal(serNo[10]));
        stock.setChangeAmount(new BigDecimal(serNo[11]));
        stock.setStockName(serNo[12]);

        industryDto.setLeaderStock(stock);
        return industryDto;
    }


    public Industry toIndustry() {
        Industry industry = new Industry();
        BeanUtils.copyProperties(this, industry);
        return industry;
    }


    /**
     * <pre>
     *     从 Industry 中拷贝相应属性
     * </pre>
     *
     * @param industry 行业信息
     */
    public void copyProperties(Industry industry) {
        setIndustryId(industry.getIndustryId());
        // 更新领涨股行业主键
        getLeaderStock().setIndustryId(industry.getIndustryId());
    }
}
