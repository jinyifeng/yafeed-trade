package com.yafeed.trade.pojo.bo.stock;

import cn.hutool.core.map.MapUtil;
import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.common.enums.DateTypeEnum;
import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.entity.leaderstock.LeaderStock;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description: 领涨股业务数据处理
 * @author: yanghj
 * @time: 2020-12-17 17:38
 **/
public class LeaderStockBo {


    public static LeaderStockBo of() {
        return new LeaderStockBo();
    }


    /**
     * <pre>
     *     list 转 map
     * </pre>
     *
     * @param leaderStocks 数据库中 领涨股信息
     * @return map
     */
    private Map<String, LeaderStock> transferToLeaderStockMap(List<LeaderStock> leaderStocks) {
        if (leaderStocks == null || leaderStocks.isEmpty()) {
            return MapUtil.newHashMap();
        }
        String format = DateTypeEnum.YYYY_MM_DD.now();
        return leaderStocks.stream().collect(Collectors.toMap(m -> m.getIndustryId() + "_" + format, item -> item));
    }


    public TwoTuple<Boolean, List<LeaderStock>> parseLeaderStock(List<LeaderStock> exists, List<LeaderStock> leaderStocks) {
        TwoTuple<Boolean, List<LeaderStock>> twoTuple = new TwoTuple<>();
        if (exists == null || exists.isEmpty()) {
            twoTuple.setFirst(false);
            twoTuple.setSecond(leaderStocks);
            return twoTuple;
        }

        Map<String, LeaderStock> dtoMap = transferToLeaderStockMap(leaderStocks);

        Map<String, LeaderStock> stockMap = transferToLeaderStockMap(exists);

        for (Map.Entry<String, LeaderStock> entry : stockMap.entrySet()) {
            LeaderStock leaderStockDto = dtoMap.get(entry.getKey());
            // 拷贝属性
            copyProperties(leaderStockDto, entry.getValue());
        }
        twoTuple.setFirst(true);
        twoTuple.setSecond(leaderStocks);
        return twoTuple;
    }


    private void copyProperties(LeaderStock target, LeaderStock source) {
        target.setIndustryId(source.getIndustryId());
        target.setLeaderStockId(source.getLeaderStockId());
    }

    public LeaderStock or() {
        return new LeaderStock();
    }


}
