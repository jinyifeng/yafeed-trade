package com.yafeed.trade.pojo.bo.stock;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.entity.historystock.HistoryStock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 历史股票
 * @author: yanghj
 * @time: 2020-12-18 17:24
 **/
public class HistoryStockBo {

    private IdService idService;

    public static HistoryStockBo of(IdService idService) {
        return new HistoryStockBo(idService);
    }


    private HistoryStockBo(IdService idService) {
        this.idService = idService;
    }

    /**
     * <pre>
     *     解析 jsonArr 数组 为 对象列表
     * </pre>
     *
     * @param jsonArr json 对象数组字符串
     * @return list
     */
    public List<HistoryStock> parseToHistoryStock(String jsonArr, String stockCode, String stockName) {
        if (!JSONUtil.isJsonArray(jsonArr)) {
            return new ArrayList<>();
        }
        List<JSONObject> stockList = JSONObject.parseArray(jsonArr, JSONObject.class);
        return stockList.stream().map(m -> or(m, stockCode, stockName)).collect(Collectors.toList());
    }


    private HistoryStock or() {
        HistoryStock stock = new HistoryStock();
        stock.setHistoryStockId(idService.genId());
        return stock;
    }

    private HistoryStock or(JSONObject json, String stockCode, String stockName) {

        HistoryStock stock = or();

        stock.setStockCode(stockCode);
        stock.setStockName(stockName);

        stock.setTradeVolume(json.getLong("volume"));
        stock.setHighPrice(json.getBigDecimal("high"));
        stock.setLowPrice(json.getBigDecimal("low"));
        stock.setMa20Volume(json.getLong("ma_volume20"));
        stock.setMa20Price(json.getBigDecimal("ma_price20"));
        stock.setTradeDay(json.getDate("day"));
        stock.setClosePrice(json.getBigDecimal("close"));
        stock.setOpenPrice(json.getBigDecimal("open"));
        return stock;
    }


}
