package com.yafeed.trade.service.industry;

import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.entity.industry.Industry;
import com.yafeed.trade.pojo.dto.industry.IndustryDto;

import java.util.List;

/**
 * @description: 板块信息
 * @author: yanghj
 * @time: 2020-12-13 14:47
 */
public interface IndustryService {

    /**
     * <pre>
     *  查询最近添加的行业
     * </pre>
     *
     * @return
     */
    Industry listLastIndustry();

    /**
     * <pre>
     *     获取新浪板块信息
     * </pre>
     *
     * @return list
     */
    List<IndustryDto> fetchSinaIndustries();


    /**
     * <pre>
     *     保存行业信息
     * </pre>
     *
     * @return list
     */
    TwoTuple<Boolean, List<IndustryDto>> saveSinaIndustries();


    /**
     * <pre>
     *      添加行业信息
     * </pre>
     *
     * @param industryDtos
     */
    void insertSinaIndustries(List<IndustryDto> industryDtos);


    /**
     * <pre>
     *     更新行信息
     * </pre>
     *
     * @param industryDtos 行业信息
     */
    void updateSinaIndustries(List<IndustryDto> industryDtos);

    /**
     * <pre>
     *     刷新新浪数据
     * </pre>
     *
     * @return boolean
     */
    boolean refreshIndustryIndividualStockFromSina();


}
