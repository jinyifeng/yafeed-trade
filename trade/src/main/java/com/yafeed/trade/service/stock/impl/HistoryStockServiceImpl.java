package com.yafeed.trade.service.stock.impl;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.robert.vesta.service.intf.IdService;
import com.yafeed.trade.common.enums.DateTypeEnum;
import com.yafeed.trade.common.http.HttpUtils;
import com.yafeed.trade.common.pojo.bo.PageBean;
import com.yafeed.trade.common.utils.RateUtils;
import com.yafeed.trade.component.SinaUrl;
import com.yafeed.trade.dao.HistoryStockMapper;
import com.yafeed.trade.entity.historystock.HistoryStock;
import com.yafeed.trade.entity.historystock.HistoryStockExample;
import com.yafeed.trade.entity.industrystock.IndustryStock;
import com.yafeed.trade.pojo.bo.stock.HistoryStockBo;
import com.yafeed.trade.service.stock.HistoryStockService;
import com.yafeed.trade.service.stock.IndustryStockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 历史个股
 * @author: yanghj
 * @time: 2020-12-18 16:10
 **/
@Slf4j
@Service
public class HistoryStockServiceImpl implements HistoryStockService {

    @Resource
    private HistoryStockMapper historyStockMapper;


    private IndustryStockService industryStockService;

    /**
     * 新浪接口地址
     */
    private SinaUrl sinaUrl;


    private HttpUtils httpUtils;


    private IdService idService;

    /**
     * <pre>
     *     获取个股记录
     * </pre>
     *
     * @param stockCode 股票代码
     * @param stockName 股票名称
     * @return list
     */
    @Override
    public List<HistoryStock> fetchSinaStockInfo(String stockCode, String stockName) {
        HistoryStock lastStock = listLastStock(stockCode, "create_time desc");
        int dayLen;
        if (lastStock == null) {
            dayLen = 120;
        } else {
            // 时间相减
            dayLen = DateTypeEnum.YYYY_MM_DD.subtractDay(DateTypeEnum.YYYY_MM_DD.date(), lastStock.getTradeDay());
        }
        String url = String.format(sinaUrl.getIndividual(), stockCode, dayLen);

        String object = httpUtils.get(url, String.class);

        HistoryStockBo stockBo = HistoryStockBo.of(idService);

        return stockBo.parseToHistoryStock(object, stockCode, stockName);
    }


    /**
     * 插入股票记录
     *
     * @param historyStocks 历史个股信息
     * @return int
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertHistoryStock(List<HistoryStock> historyStocks) {
        for (HistoryStock stock : historyStocks) {
            historyStockMapper.insertSelective(stock);
        }
        return historyStocks.size();
    }


    /**
     * <pre>
     *     更新个股历史信息
     *     <p>
     *         1.从 industry_stock 中查询个股信息
     *         2.通过 com.yafeed.trade.service.stock.HistoryStockService#fetchSinaStockInfo(java.lang.String, java.lang.String) 获取个股记录
     *     </p>
     * </pre>
     *
     * @return int
     * @see HistoryStockServiceImpl#fetchSinaStockInfo(java.lang.String, java.lang.String)
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateHistoryStock(List<IndustryStock> industryStocks) {
        int sum = 0;
        RateUtils rateUtils = RateUtils.of(5, 5);
        for (IndustryStock industryStock : industryStocks) {
            List<HistoryStock> historyStocks = fetchSinaStockInfo(industryStock.getStockCode(), industryStock.getStockName());
            while (!rateUtils.hasToken()) {
                Thread.yield();
            }
            log.debug("执行");
            sum += insertHistoryStock(historyStocks);
        }
        return sum;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public PageInfo<IndustryStock> updateHistoryStock(int pageNo, int pageSize) {
        PageBean<IndustryStock> page = new PageBean<IndustryStock>(pageNo, pageSize, "stock_code desc") {
            @Override
            public List<IndustryStock> doQuery() {
                return industryStockService.listDistinctIndustryStockSymbol();
            }
        };
        PageInfo<IndustryStock> pageInfo = page.getPageInfo();

        List<IndustryStock> list = pageInfo.getList();
        updateHistoryStock(list);
        return pageInfo;
    }


    /**
     * <pre>
     *      查询个股历史记录
     * </pre>
     *
     * @param stockCode 股票代码
     * @return HistoryStock
     */
    private HistoryStock listLastStock(String stockCode, @Nullable String order) {
        HistoryStockExample example = new HistoryStockExample();
        HistoryStockExample.Criteria criteria = example.or();
        criteria.andStockCodeEqualTo(stockCode);
        example.setOrderByClause(StrUtil.isBlank(order) ? null : order);
        PageHelper.startPage(1, 1);
        List<HistoryStock> industryStocks = historyStockMapper.selectByExample(example);
        PageInfo<HistoryStock> page = new PageInfo<>(industryStocks);
        if (page.getTotal() == 0) {
            return null;
        }
        return page.getList().get(0);
    }

    @Autowired
    public void setIndustryStockService(IndustryStockService industryStockService) {
        this.industryStockService = industryStockService;
    }

    @Autowired
    public void setHttpUtils(HttpUtils httpUtils) {
        this.httpUtils = httpUtils;
    }

    @Autowired
    public void setSinaUrl(SinaUrl sinaUrl) {
        this.sinaUrl = sinaUrl;
    }

    @Autowired
    public void setIdService(IdService idService) {
        this.idService = idService;
    }
}
