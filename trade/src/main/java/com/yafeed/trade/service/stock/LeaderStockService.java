package com.yafeed.trade.service.stock;

import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.entity.leaderstock.LeaderStock;

import java.util.List;

/**
 * @description: 领涨股
 * @author: yanghj
 * @time: 2020-12-18 15:57
 **/
public interface LeaderStockService {



    /**
     * <pre>
     *     保存领涨股
     * </pre>
     *
     * @param leaderStocks 领涨股
     */
    void insertSinaLeaderStock(List<LeaderStock> leaderStocks);


    /**
     * <pre>
     *     更新行业领涨股信息
     * </pre>
     *
     * @param leaderStocks 领涨股对象
     */
    void updateLeaderStock(List<LeaderStock> leaderStocks);

    /**
     * <pre>
     *     保存行业领涨股信息
     * </pre>
     *
     * @param leaderTuple 领涨股元组
     */
    void saveLeaderStock(TwoTuple<Boolean, List<LeaderStock>> leaderTuple);
}
