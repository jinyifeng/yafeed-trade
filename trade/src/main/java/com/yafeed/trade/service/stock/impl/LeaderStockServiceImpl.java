package com.yafeed.trade.service.stock.impl;

import com.yafeed.trade.common.enums.DateTypeEnum;
import com.yafeed.trade.common.tuple.TwoTuple;
import com.yafeed.trade.dao.LeaderStockMapper;
import com.yafeed.trade.entity.leaderstock.LeaderStock;
import com.yafeed.trade.entity.leaderstock.LeaderStockExample;
import com.yafeed.trade.pojo.bo.stock.LeaderStockBo;
import com.yafeed.trade.service.stock.LeaderStockService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @description: 领涨股
 * @author: yanghj
 * @time: 2020-12-18 15:58
 **/
@Service
public class LeaderStockServiceImpl implements LeaderStockService {

    /**
     * 领涨股
     */
    @Resource
    private LeaderStockMapper leaderStockMapper;

    /**
     * <pre>
     *     新增领涨股
     * </pre>
     *
     * @param leaderStocks 领涨股信息
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertSinaLeaderStock(List<LeaderStock> leaderStocks) {
        for (LeaderStock leaderStock : leaderStocks) {
            leaderStockMapper.insertSelective(leaderStock);
        }
    }

    /**
     * <pre>
     *     更新行业领涨股信息
     * </pre>
     *
     * @param leaderStocks 领涨股对象
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateLeaderStock(List<LeaderStock> leaderStocks) {
        for (LeaderStock leaderStock : leaderStocks) {
            leaderStockMapper.updateByPrimaryKeySelective(leaderStock);
        }
    }

    /**
     * <pre>
     *     保存行业领涨股信息
     * </pre>
     *
     * @param leaderTuple 领涨股元组
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveLeaderStock(TwoTuple<Boolean, List<LeaderStock>> leaderTuple) {
        Date date = DateTypeEnum.YYYY_MM_DD.date();
        LeaderStockExample leaderStockExample = new LeaderStockExample();
        LeaderStockExample.Criteria criteria = leaderStockExample.createCriteria();
        criteria.andCreateTimeGreaterThan(date);
        List<LeaderStock> exists = leaderStockMapper.selectByExample(leaderStockExample);

        LeaderStockBo leaderStockBo = LeaderStockBo.of();
        TwoTuple<Boolean, List<LeaderStock>> twoTuple = leaderStockBo.parseLeaderStock(exists, leaderTuple.getSecond());

        if (twoTuple.getFirst()) {
            // 修改
            updateLeaderStock(twoTuple.getSecond());
            return;
        }
        // 新增
        insertSinaLeaderStock(twoTuple.getSecond());


    }
}
