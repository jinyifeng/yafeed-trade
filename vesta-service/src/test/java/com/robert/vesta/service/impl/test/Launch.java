package com.robert.vesta.service.impl.test;

import com.robert.vesta.service.impl.IdServiceImpl;
import com.robert.vesta.service.impl.provider.PropertyMachineIdsProvider;

/**
 * @description: 运行测试
 * @author: yanghj
 * @time: 2020-12-23 10:45
 **/
public class Launch {
    public static void main(String[] args) {
        IdServiceImpl service = new IdServiceImpl();
        PropertyMachineIdsProvider propertyMachineIdsProvider = new PropertyMachineIdsProvider();
        long[] arr = {1L, 2L, 3L, 4L};
        propertyMachineIdsProvider.setMachineIds(arr);
        service.setMachineIdProvider(propertyMachineIdsProvider);
        service.init();
        System.out.println(service.genId());
    }

}


